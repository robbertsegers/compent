var Compent = angular.module('Compent', [
	"ui.router",
	"ui.bootstrap",
	"ngFileUpload"
	])

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $provide) {

	$urlRouterProvider.otherwise("/");
		$stateProvider
			.state('home', {
				url: "/",
				controller: "HomeCtrl",
				templateUrl: "view/home.html"
			})
			.state('profile', {
				url: "/profile",
				controller: "ProfileCtrl",
				templateUrl: "view/profile.html"
			})
			.state('erndswun', {
				url: "/erndswun",
				controller: "ProfileCtrl",
				templateUrl: "view/erndswun.html"
			})
			.state('help', {
				url: "/help",
				templateUrl: "view/help.html"
			})
			.state('contact', {
				url: "/contact",
				controller: "ProfileCtrl",
				templateUrl: "view/contact.html"
			})
			.state('pro', {
				url: "/pro",
				templateUrl: "view/pro.html"
			})
			.state('business', {
				url: "/business",
				templateUrl: "view/business.html"
			})
});

