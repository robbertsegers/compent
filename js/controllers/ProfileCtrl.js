//https://github.com/danialfarid/ng-file-upload

var Compent = angular.module('Compent')

	.controller('ProfileCtrl', function ($scope, $http, Upload) {

		var scrollContent = function() {
			$window.scrollTo(0, 0);
		};

		$scope.name="";
		$scope.title="";
		$scope.summary="";

		$scope.skills = ["skill"];
		$scope.addSkill =  function addSkill() {
			$scope.skills.push(" ");
		}
		$scope.competences = [
			"leadership",
			"organisational",
			"communicative",
			"creative",
			"resilience",
			"team Player",
			"problem solving",
			"decisive",
			"planning",
			"prioritize",
			"processing information",
			"analytical",
			"competitive",
			"discipline",
			"ideas formation",
			"input",
			"intellect",
			"empathy",
			"focus",
			"inquisitiveness",
			"persuasiveness",
			"positivism",
			"achieving",
			"self-assurance"
		];
		$scope.selectedcomp = ["competence"];
		$scope.setcompetence = function setcompetence(comp, index){
			$scope.selectedcomp[index] = comp;
		};

		//Number of personality traits
		$scope.traits = ["trait"];
		$scope.showaddpers = true;
		$scope.addTrait =  function addTrait() {
			$scope.traits.push(" ");
			if ($scope.traits.length == 3) {
				$scope.showaddpers = false;
			};
		}
		//traits array
		$scope.persontraits = [
			"integrity",
			"honesty",
			"loyalty",
			"respectfulness",
			"responsibility",
			"humilty",
			"compassion",
			"fairness",
			"forgiveness",
			"authenticity",
			"courageousness",
			"generosity",
			"kindness"
		];
		$scope.selectedtrait = ["Personality"];
		$scope.settrait = function settrait(trait, index){
			$scope.selectedtrait[index] = trait;
		};

		$scope.edex = ["edu"];
		$scope.addEdex =  function addSkill() {
			$scope.edex.push(" ");
		}

		$scope.years = [2015];
		for (i = 2014; i > 1900; i--) {
			$scope.years.push(i);
		}

		$scope.sent = true;
		$scope.contact = false;
		$scope.contactf = function contactf(){
			$scope.contact = true;
			$scope.sent = false;
		}

		// upload on file select or drop
	    $scope.upload = function (file) {
	        Upload.upload({
	            url: '/images/',
	            file: file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	        }).success(function (data, status, headers, config) {
	            console.log('file ' + config.file.name + '  uploaded. Response: ' + data);
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })
	    };

	});






